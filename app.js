const express = require("express");
const multer = require("multer");
const cors = require("cors");
const bodyParser = require("body-parser");
const { createServer } = require("http");
const { Server } = require("socket.io");
const { uploadImage } = require("./lib/cloudinary");

require("dotenv").config();

const passport = require("./lib/passport");
const authController = require("./controllers/auth");
const usersController = require("./controllers/users");
const gamesController = require("./controllers/games");
const anonymController = require("./controllers/anonym_messages");

const app = express();
const port = process.env.PORT || 3001;
app.use(cors());
app.use(bodyParser.json());
app.use("/public", express.static("public"));
app.use(passport.initialize());

const httpServer = createServer(app);
const io = new Server(httpServer, {
  cors: {
    origin: process.env.SOCKET_IO_CONNECT_LOCALHOST,
    methods: ["GET", "POST"],
  },
});

const storage = multer.diskStorage({
  destination: (req, file, cb) => {
    cb(null, "./public/img/");
  },
  filename: (req, file, cb) => {
    cb(null, file.originalname);
  },
});

const upload = multer({ storage });

app.get("/users", usersController.users);

app.get("/users-page", usersController.usersPagination);

app.get(
  "/user",
  passport.authenticate("jwt", { session: false }),
  usersController.user
);
app.post(
  "/edit-profile",
  passport.authenticate("jwt", { session: false }),
  upload.single("profileImage"),
  usersController.editProfile
);

app.post("/register", authController.register);
app.post("/login", authController.login);
app.get(
  "/logout",
  passport.authenticate("jwt", { session: false }),
  authController.logout
);

app.get("/game-list", gamesController.gameList);
app.get("/game-detail/:label", gamesController.gameDetail);
app.post(
  "/games/play-game",
  passport.authenticate("jwt", { session: false }),
  gamesController.playGame
);
app.put(
  "/games/update-score",
  passport.authenticate("jwt", { session: false }),
  gamesController.updateScore
);
app.get("/leaderboard/:id", gamesController.leaderboard);

app.get("/anonym-messages", anonymController.messages);
app.post("/anonym-messages", anonymController.addMessages);

httpServer.listen(port, () => {
  console.log(`Example app listening on port ${port}`);
});
