const { Anonym_messages } = require("../../models");

exports.messages = async (req, res) => {
  const data = await Anonym_messages.findAll();

  res.json({
    status: "SUCCESS",
    data,
  });
};

exports.addMessages = async (req, res) => {
  const { tittle, text } = req.body;

  try {
    await Anonym_messages.create({ tittle, text });
    const response = await Anonym_messages.findAll();

    res.json({
      status: "SUCCES",
      data: response,
    });
  } catch (error) {
    res.status(501).json({
      status: "FAILED",
    });
  }
};
