const bcrypt = require("bcrypt");
const jwt = require("jsonwebtoken");
const { Users } = require("../../models");

exports.register = async (req, res) => {
  const { name, email, password } = req.body;
  const passwordBcrypt = await bcrypt.hash(password, 10);

  const emailAlready = await Users.findOne({
    where: { email },
  });

  if (emailAlready) {
    res.status(402).json({
      status: "FAILED",
      message: "Email is already!",
    });
    return;
  }

  await Users.create({
    name,
    email,
    password: passwordBcrypt,
    logoutAt: new Date(),
  });

  res.json({
    status: "SUCCESS",
    message: "Register success",
  });
};

exports.login = async (req, res) => {
  const { email, password } = req.body;

  const user = await Users.findOne({
    where: { email },
  });

  if (!user) {
    res.status(403).json({
      status: "FAILED",
      message: "Email Salah!",
    });
    return;
  }

  const passwordValidation = await bcrypt.compare(password, user.password);
  if (!passwordValidation) {
    res.status(402).json({
      status: "FAILED",
      message: "Password Salah!",
    });
    return;
  }

  await user.update({ isOnline: true });
  await user.save();

  let isVerify = true;
  if (!user.bio || user.imageURL) isVerify = false;

  const { id, name, role } = user;

  const token = jwt.sign(
    {
      id,
    },
    "secret"
  );

  const response = {
    name,
    role,
    isVerify,
    accessToken: token,
  };

  res.json({
    status: "SUCCESS",
    data: response,
  });
};

exports.logout = async (req, res) => {
  const user = await Users.findOne({
    where: { id: req.user.id },
  });

  await user.update({ isOnline: false, logoutAt: new Date() });
  await user.save();

  res.json({
    status: "SUCCESS",
  });
};
