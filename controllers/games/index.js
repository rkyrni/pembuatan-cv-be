const { Games, Users, Scores } = require("../../models");

exports.gameList = async (req, res) => {
  const gameList = await Games.findAll();

  res.json({
    status: "SUCCESS",
    data: gameList,
  });
};

exports.gameDetail = async (req, res) => {
  const label = req.params.label;

  const gameDetail = await Games.findOne({
    where: { label },
  });

  if (!gameDetail) {
    res.status(404).json({
      status: "FAILED",
    });

    return;
  }

  res.json({
    status: "SUCCESS",
    data: gameDetail,
  });
};

exports.playGame = async (req, res) => {
  const { game_id } = req.body;
  const user = req.user;

  const userGame = await Users.findOne({
    where: { id: user.id },
  });

  const isAlreadyPlay = await Scores.findOne({
    where: { game_id, user_id: userGame.id },
  });

  if (!isAlreadyPlay) {
    const newScore = await Scores.create({
      user_id: userGame.id,
      game_id,
      score: 0,
    });

    newScore.status = false;

    res.json({
      message: "SUCCESS",
      data: newScore,
    });
  } else {
    isAlreadyPlay.status = true;
    res.json({
      message: "SUCCES",
      data: isAlreadyPlay,
    });
  }
};

exports.updateScore = async (req, res) => {
  const userGame = await Users.findOne({
    where: { id: req.user.id },
  });

  if (!userGame) {
    res.status(404).json({
      message: "FAILED",
    });
    return;
  }

  const scoreUser = await Scores.findOne({
    where: { user_id: userGame.id, game_id: req.body.game_id },
  });

  if (!scoreUser) {
    res.status(404).json({
      message: "FAILED",
    });
    return;
  }

  await scoreUser.update({ score: Number(req.body.score) });
  await scoreUser.save();

  res.json(scoreUser);
};

exports.leaderboard = async (req, res) => {
  const id = Number(req.params.id);

  const leaderboard = await Scores.findAll({
    where: { game_id: id },
  });

  if (!leaderboard) {
    res.status(404).json({
      status: "Failed",
    });
    return;
  }

  const compare = (a, b) => {
    if (Number(a.score) > Number(b.score)) {
      return -1;
    }
    if (Number(a.score) < Number(b.score)) {
      return 1;
    }
    return 0;
  };

  const payload = leaderboard.sort(compare);

  res.json({
    status: "SUCCESS",
    data: payload,
  });
};
