const { uploadImage } = require("../../lib/cloudinary");
const { Users } = require("../../models");
const fs = require("fs");
const { Op, Sequelize } = require("sequelize");

exports.user = async (req, res) => {
  const user = await Users.findOne({
    where: { id: req.user.id },
  });

  let isVerify = true;
  if (!user.bio || user.imageURL) isVerify = false;

  const { name, bio, imageURL, role, email } = user;
  const response = { name, bio, imageURL, role, email, isVerify };

  res.json(response);
};

exports.editProfile = async (req, res) => {
  const { name, bio, isDeleteProfile } = req.body;

  try {
    const user = await Users.findOne({
      where: { id: req.user.id },
    });
    console.log(req.body);
    console.log(!req.file);

    if (!req.file && isDeleteProfile) {
      await user.update({ name, bio, imageURL: null });
      await user.save();

      res.json({
        staus: "SUCCESS",
        data: user,
      });

      return;
    }

    if (!req.file) {
      await user.update({ name, bio });
      await user.save();

      res.json({
        staus: "SUCCESS",
        data: user,
      });

      return;
    }

    const upload = await uploadImage(req.file.path);
    await user.update({ name, bio, imageURL: upload.secure_url });
    await user.save();
    fs.unlinkSync(`public/img/${req.file.filename}`);

    res.json({
      staus: "SUCCESS",
      data: user,
    });
  } catch (err) {
    res.status(401).json({
      status: "FAILED",
    });
  }
};

exports.users = async (req, res) => {
  const users = await Users.findAll();

  res.json({
    status: "SUCCESS",
    data: users,
  });
};

exports.usersPagination = async (req, res, next) => {
  const searchName = req.query.name || "";
  const currentPage = parseInt(req.query.page) || 1;
  const limitPage = parseInt(req.query.limit) || 8;
  const end = currentPage * limitPage;

  try {
    const users = await Users.findAndCountAll({
      where: {
        name: Sequelize.where(Sequelize.fn("lower", Sequelize.col("name")), {
          [Op.like]: `%${searchName.toLowerCase()}%`,
        }),
        role: "user",
      },
      attributes: [
        "id",
        "name",
        "bio",
        "email",
        "imageURL",
        "isOnline",
        "logoutAt",
        "createdAt",
      ],
      order: [["name", "asc"]],
      limit: limitPage,
      offset: (currentPage - 1) * limitPage,
    });

    const pagination = {};
    pagination.totals_pages = Math.ceil(users.count / limitPage);

    if (end < users.count) {
      pagination.next = currentPage + 1;
    }

    if ((currentPage - 1) * limitPage > 0) {
      pagination.prev = currentPage - 1;
    }

    res.json({
      status: "SUCCES",
      pagination,
      data: users.rows,
    });
  } catch (error) {
    res.status(404).json({
      status: "FAILED",
    });
  }
};
